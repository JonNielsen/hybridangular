import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UpgradeModule } from '@angular/upgrade/static';
import { HeroDetailComponent } from './components/herodetails/herodetails.component';
import { downgradeComponent } from '@angular/upgrade/static';
import * as angular from 'angular';
import { AppRoutingModule } from './app-routing.module';

angular.module('appold')
  .directive(
    'heroDetail',
    downgradeComponent({ component: HeroDetailComponent } as angular.IDirectiveFactory)
  )
  .directive('myCustomer', function() {
    return {
      template: 'This is a test'
    };
  });


@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent
  ],
  imports: [
    BrowserModule,
    UpgradeModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [HeroDetailComponent]
})
export class AppModule { 
  constructor(private readonly upgrade: UpgradeModule) {}
  ngDoBootstrap() {
    // this.upgrade.bootstrap(document.body, ["appold"], { strictDi: true });
    // angular.bootstrap(document.body, ['appold']);
  }
}
